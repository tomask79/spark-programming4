case class Team(
	name: String,
	country: String);

case class Match(
	t1: String,
	t2: String,
	g1: Int,
	g2: Int);

val teamsRDD = sc.textFile("/tests/teams.txt");
val matchesRDD = sc.textFile("/tests/matches.txt");

val teamsObjects = teamsRDD.map(line=>(line.split(",")(0), Team(line.split(",")(0), line.split(",")(1))));
val matchesObjects = matchesRDD.map(line=>Match(line.split(",")(0).split("-")(0),
						line.split(",")(0).split("-")(1),
						line.split(",")(1).split(":")(0).toInt,
						line.split(",")(1).split(":")(1).toInt));


val teamGoalsPairs = matchesObjects.flatMap(m=>Array((m.t1, m.g1),(m.t2, m.g2)));

val sortedTeams = teamGoalsPairs.reduceByKey((a,b)=>a+b).map(teamGoals=>(teamGoals._2,teamGoals._1)).sortByKey(false).map(tG=>(tG._2,tG._1));

val thirdScoringTeam = sortedTeams.take(3)(2);

val thirdMatches = matchesObjects.filter(m=>(m.t1.equals(thirdScoringTeam._1) || m.t2.equals(thirdScoringTeam._1)));

// Task1:
val teamCountry = teamsObjects.filter(t=>t._1.equals(thirdScoringTeam._1)).map(t=>t._2);
println(teamCountry.name+" "+teamCountry.country);

// Task2:
thirdMatches.map(m=>m.t1+"-"+m.t2+" "+m.g1+":"+m.g2).saveAsTextFile("/tests/thirdMatches3.txt");

System.exit(0);
