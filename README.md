# Spark programming 4 #

I tried another task during learning on [Hortonworks Spark Certification](https://hortonworks.com/services/training/certification/hdp-certified-spark-developer/).
Let's have **UEFA Champions League**.

First input file, **teams.txt**:

	Barcelona,Spain
	RealMadrid,Spain
	Juventus,Italy
	Chelsea,England
	ManchesterCity,England
	ManchesterUnited,England
	LazioRoma,Italy
	PSG,France

Second input file, **matches.txt**

	Barcelona-PSG,3:1
	RealMadrid-Juventus,5:0
	Chelsea-LazioRoma,1:1
	Barcelona-ManchesterCity,2:2
	PSG-RealMadrid,3:3
	ManchesterUnited-PSG,2:1
	LazioRoma-Juventus,5:0


**Task1:** 

Who was the **third** best scoring team?

**Task2:**

Save matches of the team from Task1 into thirdMatches.txt file.

It doesn't look difficult, but there are few interesting tasks in the solution of the mentioned problems:

* Howto emit multiple key values in one map function call
* Howto get N-th item from the RDD

Solution:

1) Parsing the inputs:

	case class Team(
		name: String,
		country: String);

	case class Match(
		t1: String,
		t2: String,
		g1: Int,
		g2: Int);

	val teamsRDD = sc.textFile("/tests/teams.txt");
	val matchesRDD = sc.textFile("/tests/matches.txt");

	val teamsObjects = teamsRDD.map(line=>(line.split(",")(0), Team(line.split(",")(0), line.split(",")(1))));
	val matchesObjects = matchesRDD.map(line=>Match(line.split(",")(0).split("-")(0),
						line.split(",")(0).split("-")(1),
						line.split(",")(1).split(":")(0).toInt,
						line.split(",")(1).split(":")(1).toInt));


As usual, we're creating RDD's of objects.

2) Sorting the teams according the scored goals.

	val teamGoalsPairs = matchesObjects.flatMap(m=>Array((m.t1, m.g1),(m.t2, m.g2)));

	val sortedTeams = teamGoalsPairs.reduceByKey((a,b)=>a+b).map(teamGoals=>(teamGoals._2,teamGoals._1)).sortByKey(false).map(tG=>(tG._2,tG._1));

	val thirdScoringTeam = sortedTeams.take(3)(2);

**The idea is:** 

We need to **break the matches into the key-value pairs** in the format (team,scoredGoals). And since
one match is between two teams then we need to emit two key value pairs in one function call...
Optimal solution in scala is to generate static Array and make it flat...
Check **teamGoalsPairs** assignment.

Now howto **get N-th element from RDD?** I've seen two approaches so far:

* Using of zipWithIndex
* If N is small then take first N elements to driver program and tail it.

I went with second solution, see **thirdScoringTeam** assignment. 

Sorted teams by scored goals (sortedTeams.collect()):

	18/01/27 15:42:54 INFO DAGScheduler: ResultStage 4 (collect at <console>:38) finished in 0,048 s
	18/01/27 15:42:54 INFO DAGScheduler: Job 1 finished: collect at <console>:38, took 0,168392 s
	res0: Array[(String, Int)] = Array((RealMadrid,8), (LazioRoma,6), (PSG,5), (Barcelona,5), (ManchesterUnited,2), (ManchesterCity,2), (Chelsea,1), (Juventus,0))

3) Printing the results of the tasks

	// Task1:
	val teamCountry = teamsObjects.filter(t=>t._1.equals(thirdScoringTeam._1)).map(t=>t._2);
	println(teamCountry.name+" "+teamCountry.country);

	// Task2:
	thirdMatches.map(m=>m.t1+"-"+m.t2+" "+m.g1+":"+m.g2).saveAsTextFile("/tests/thirdMatches3.txt");

## Result ##

Task 1:

	18/01/27 16:09:53 INFO DAGScheduler: ResultStage 4 (take at <console>:37) finished in 0,053 s
	18/01/27 16:09:53 INFO DAGScheduler: Job 1 finished: take at <console>:37, took 0,167460 s
	thirdScoringTeam: (String, Int) = (PSG,5)

Task 2:

	[root@sandbox ~]# hdfs dfs -cat /tests/thirdMatches5.txt/part-00001
	PSG-RealMadrid 3:3
	ManchesterUnited-PSG 2:1
	[root@sandbox ~]# 
	
Best regards

Tomas